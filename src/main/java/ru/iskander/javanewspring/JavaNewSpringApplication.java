package ru.iskander.javanewspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaNewSpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(JavaNewSpringApplication.class, args);
    }

}
