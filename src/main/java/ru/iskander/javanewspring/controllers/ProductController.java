package ru.iskander.javanewspring.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import ru.iskander.javanewspring.models.Product;
import ru.iskander.javanewspring.services.ProductService;

import java.io.IOException;
import java.security.Principal;

@Controller
@RequiredArgsConstructor
public class ProductController {
    private final ProductService productService;


    @GetMapping("/")
    public String products(@RequestParam(name = "title", required = false) String title, Principal principal, Model model) {
        model.addAttribute("products", productService.listProducts(title));
        model.addAttribute("user", productService.getUserByPrincipal(principal));
        return "products";
    }

    @GetMapping("/product/{id}")
    public String prodInfo(@PathVariable Long id, Model model) {
        Product product = productService.getProductById(id);
        model.addAttribute("product", product);
        model.addAttribute("images", product.getImages());
        return "product-info";
    }




    //    @PostMapping("/product/create")
//    public String createProduct(@RequestParam("file1") MultipartFile file1, @RequestParam("file2") MultipartFile file2,
//                                @RequestParam("file3") MultipartFile file3, Product product, Principal principal) throws IOException {
//        productService.saveProduct(principal, product, file1, file2, file3);
//        return "redirect:/";
//
//    }
    @PostMapping("/product/create")
    public String createProduct(Product product, Principal principal) throws IOException {
        productService.saveProduct(principal, product);
        return "redirect:/";

    }
    @GetMapping("/product/update/{productid}")
    public String updateProduct(@PathVariable String productid, Model model, Principal principal, Product product) throws IOException{
        model.addAttribute("product", productService.getProductById(Long.valueOf(productid)));
        return  "product-update";
    }

    @PostMapping("/product/update/{id}")
    public String updateProd(@PathVariable Long id, String description) {
        Product product = productService.getProductById(id);
        product.setDescription(description);
        productService.updateProduct(product);
        return "redirect:/";

    }

    @PostMapping("/product/delete/{id}")
    public String deleteProd(@PathVariable Long id) {
        productService.deleteProd(id);
        return "redirect:/";

    }
}
