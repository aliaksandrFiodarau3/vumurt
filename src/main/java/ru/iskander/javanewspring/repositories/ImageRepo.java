package ru.iskander.javanewspring.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.iskander.javanewspring.models.Image;

public interface ImageRepo extends JpaRepository<Image, Long> {

}
