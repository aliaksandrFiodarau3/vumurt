package ru.iskander.javanewspring.repositories;

import org.hibernate.metamodel.model.convert.spi.JpaAttributeConverter;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.iskander.javanewspring.models.Product;

import java.util.List;

public interface ProductRepo extends JpaRepository<Product, Long> {       //получаем доступ к SOUD
    List<Product> findByTitle(String title);
}
